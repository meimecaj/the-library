# The Library

A small library to keep track of your books.

## Install
This project uses [node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/). Go check them out if you don't have them locally installed.
<br>In the root of project, run:
```
npm install
```

## Usage
```
npm start
```


Mei Meçaj
